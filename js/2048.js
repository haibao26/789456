  //最高分
  var high = 0;
  //得分
  var score = 0;
  var divs= document.querySelectorAll("#box>div");
  var arr = [[],[],[],[]];
  var num=0;

  /*
  主逻辑
  */

  //初始化
  init();
  //上下左右的监听事件
  window.onkeydown = function(e){

      switch(e.keyCode){
          case 37 :  isFull(); left();bgcolor();     break;//左
          case 38 :  isFull(); up();bgcolor();     break;//上
          case 39 :  isFull(); right();bgcolor();     break;//右
          case 40 :  isFull(); down(); bgcolor();     break;//下
          case 73 :  init(); break;//初始化
      } 

  }
 //随机产生一个数字
function rand(){
var x=Math.floor(Math.random()*4);
var y=Math.floor(Math.random()*4);
if(arr[x][y].innerHTML == ""){
  arr[x][y].innerHTML = Math.random() > 0.5 ? 2 : 4;
}else{
  rand();
}
}

  //初始化
  function init(){
      for(var i = 0;i <arr.length;i++){
          for(var j = 0;j <arr.length;j++){
              arr[i][j] = divs[num];
              arr[i][j].innerHTML = '';
              num++;
          }
      }
      //初始化遍历元素
      num = 0;
      //更新最高分
      if(score>high)
          document.getElementsByClassName('score')[0].innerHTML = high = score;
      //初始化得分
      document.getElementsByClassName('score')[1].innerHTML = score = 0;
      //游戏开始产生2个随机数
      rand();
      rand();
      bgcolor();
  }

  //加分
  function addScore(s) {
      score += s;
      document.getElementsByClassName('score')[1].innerHTML = score;
  }

  //判断方格是否全部填满
  function isFull(){
      var bool=true;
      for(var i = 0;i <arr.length;i++){
          for(var j = 0;j <arr.length;j++){
              if(arr[i][j].innerHTML == "" ){
              bool = false;
              }
          }
      }
      if(bool){
      isChange();
      }else{
          rand();
      }
  }

  //判断方格是否还能移动
  function  isChange(){
      var bool = true;
      for(var i = 0;i < arr.length-1 ;i++){
          for(var j = 0;j< arr.length-1;j++){
              if(arr[i][j].innerHTML == arr[i][j+1].innerHTML || arr[i][j].innerHTML == arr[i+1][j].innerHTML || arr[i+1][j].innerHTML == arr[i+1][j+1].innerHTML || arr[i][j+1].innerHTML == arr[i+1][j+1].innerHTML  ){
                  bool = false;
              }
          }
      }
      if(bool){
          alert("再来一次");
          init();
      }
  }

  //不同的数字添加不同的背景颜色
  function bgcolor(){
      for(var i = 0;i <arr.length;i++){
          for(var j = 0;j <arr.length;j++){
          
          switch(arr[i][j].innerHTML){
              case '2': arr[i][j].style.backgroundColor = "#EEE4DA" ;break;
              case '4': arr[i][j].style.backgroundColor = "#EDE0C8" ;break;
              case '8': arr[i][j].style.backgroundColor = "#F2B179" ;break;
              case '16': arr[i][j].style.backgroundColor = "#F59563" ;break;
              case '32': arr[i][j].style.backgroundColor = "#F67C5F" ;break;
              case '64': arr[i][j].style.backgroundColor = "#F65E3B" ;break;
              case '128': arr[i][j].style.backgroundColor = "#EDCF72" ;break;
              case '256': arr[i][j].style.backgroundColor = "#EDCC61" ;break;
              case '512': arr[i][j].style.backgroundColor = "#EDC850" ;break;
              case '1024': arr[i][j].style.backgroundColor = "yellowgreen" ;break;
              case '2048': arr[i][j].style.backgroundColor = "perple" ;
                          init();
                          alert('游戏胜利');
                          break;
              default:  arr[i][j].style.backgroundColor = "#CDC1B4" ;break;     

          }
          }
      }

  }

  //上下左右按下执行的函数 
  //右             
  function right(){
      for(var i = 0;i <4;i++){
          for(var j = 0;j <4;j++){
              if( j<3&&arr[i][j].innerHTML !=""&& arr[i][j+1].innerHTML==""){
                  arr[i][j+1].innerHTML = arr[i][j].innerHTML;
                  arr[i][j].innerHTML="";
                  right();
              }else if(j<3&&arr[i][j].innerHTML !=""&& arr[i][j].innerHTML == arr[i][j+1].innerHTML){
                  arr[i][j+1].innerHTML *=2;
                  addScore(arr[i][j+1].innerHTML*1)
                  arr[i][j].innerHTML ="";
              } 
          
          }
      }
  }

  //左
  function left(){
      for(var i = 0;i <4;i++){
          for(var j = 0;j <4;j++){
              if( j>0&&arr[i][j].innerHTML !=""&& arr[i][j-1].innerHTML==""){
                  arr[i][j-1].innerHTML = arr[i][j].innerHTML;
                  arr[i][j].innerHTML="";
                  left();
              }else if(j>0&&arr[i][j].innerHTML !=""&& arr[i][j].innerHTML == arr[i][j-1].innerHTML){
                  arr[i][j-1].innerHTML *=2;
                  addScore(arr[i][j-1].innerHTML*1)
                  arr[i][j].innerHTML ="";
              } 
          
          }
      }
  }
//下
function down(){
      for(var i = 0;i <4;i++){
          for(var j = 0;j <4;j++){
              if( i<3&&arr[i][j].innerHTML !=""&& arr[i+1][j].innerHTML==""){
                  arr[i+1][j].innerHTML = arr[i][j].innerHTML;
                  arr[i][j].innerHTML="";
                  down();
              }else if(i<3&&arr[i][j].innerHTML !=""&& arr[i][j].innerHTML == arr[i+1][j].innerHTML){
                  arr[i+1][j].innerHTML *=2;
                  addScore(arr[i+1][j].innerHTML*1)
                  arr[i][j].innerHTML ="";
              } 
          
          }
      }
  }

  //上
  function up(){
      for(var i = 0;i <4;i++){
          for(var j = 0;j <4;j++){
              if( i>0&&arr[i][j].innerHTML !=""&& arr[i-1][j].innerHTML==""){
                  arr[i-1][j].innerHTML = arr[i][j].innerHTML;
                  arr[i][j].innerHTML="";
                  up();
              }else if(i>0&&arr[i][j].innerHTML !=""&& arr[i][j].innerHTML == arr[i-1][j].innerHTML){
                  arr[i-1][j].innerHTML *=2;
                  addScore(arr[i-1][j].innerHTML*1)
                  arr[i][j].innerHTML ="";
              } 
          
          }
      }
  }